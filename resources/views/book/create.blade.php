@extends('layouts.app')

@section('title')
	Nieuw boek toevoegen
@endsection

@section('tools')
<li role="navigation">
	<a onClick="window.history.back()">
		<i class="fa fa-arrow-left"></i>&nbspTerug
	</a>
</li>
@endsection

@section('content')
{!! Form::open(['route' => ['book.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="col-sm-6">
		{!! Form::label('isbn', 'ISBN', ['class' => 'control-label']) !!}
		{!! Form::text('isbn', null, ['class' => 'form-control', 'placeholder' => 'ISBN Nummer']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('title', 'Titel', ['class' => 'control-label']) !!}
		{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'De titel hier']) !!}
	</div>

</div>
<div id="comment">
	<br>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-primary">
			Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}
@endsection
@section('scripts')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>


	<script type="text/javascript">




	function checkInput(){
	var search = document.getElementById('isbn').value;
	var searchLength = search.length;
	if (searchLength == 10 || searchLength == 13 ){
	document.getElementById('title').value = ""

	$.ajax({
	url: "https://www.googleapis.com/books/v1/volumes?q=isbn:" + search,
	dataType: "json",

	success: function(data) {
	if($(data.items).length > 0){
	$('#title').val(data.items[0].volumeInfo.title);
	$('#title').attr('readonly','true');
	$('#isbn').css({"border-color": "inherit", "box-shadow": "inherit"});
	document.getElementById('comment').innerHTML="";
	} else {
	$('#isbn').css({"border-color": "red", "box-shadow": "0px 0px 5px #ff0000"});
	$('#title').removeAttr('readonly');
	document.getElementById('comment').innerHTML="Let op! Dit ISBN wordt niet gevonden in de Google Books API.";
	}

	},

	type: 'GET'
	});
	}else if(searchLength > 13 || (searchLength == 11 || searchLength == 12)){
	document.getElementById('comment').innerHTML="Een ISBN bestaat uit 10 of 13 cijfers.";
		$('#title').val('');
		$('#title').attr('readonly','true');
	$('#isbn').css({"border-color": "red", "box-shadow": "0px 0px 5px #ff0000"});

	}else if(searchLength < 10){
		$('#title').attr('readonly','true');
	}

	}

	document.getElementById('isbn').addEventListener('keyup', checkInput, true)
	</script>
@endsection